<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

use Stringable;

/**
 * BlocklistInformationInterface interface file.
 * 
 * This interface represents the results for a given queried domain.
 * 
 * @author Anastaszor
 */
interface BlocklistInformationInterface extends Stringable
{
	
	/**
	 * Gets whether the queried domain is blocked.
	 * 
	 * @return boolean
	 */
	public function isBlocked() : bool;
	
	/**
	 * Gets the reason from which the queried domain is blocked.
	 * 
	 * @return string
	 */
	public function getReason() : string;
	
}
