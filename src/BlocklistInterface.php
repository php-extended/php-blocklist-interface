<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * BlocklistInterface interface file.
 * 
 * This interface represents a list that evaluates whether a given domain
 * should be blocked for any reason.
 * 
 * @author Anastaszor
 */
interface BlocklistInterface extends Stringable
{
	
	/**
	 * Gets whether the given string, interpreted as domain name, is a valid
	 * domain name that is not blocked.
	 * 
	 * @param string $domain
	 * @return BlocklistInformationInterface
	 */
	public function isAllowed(string $domain) : BlocklistInformationInterface;
	
	/**
	 * Gets whether the given uri's domain is allowed and is not blocked.
	 * 
	 * @param UriInterface $uri
	 * @return BlocklistInformationInterface
	 */
	public function isUriAllowed(UriInterface $uri) : BlocklistInformationInterface;
	
}
